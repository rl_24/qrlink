package me.rl24.techtime.util;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import me.rl24.techtime.impl.QRLinkComponent;

/**
 * QRLink configuration class
 */
public class Settings {

    /**
     * Enum for use if the plugin is expanded and requires more configuration
     */
    private enum ConfigKey {
        URLSHORTENER
    }

    private static PluginSettings settings;

    /**
     * Get the plugin settings instance, newly created if not already existing
     * @return The plugin settings instance
     */
    private static PluginSettings getSettings() {
        if (settings == null)
            settings = QRLinkComponent.getInstance().getPluginSettingsFactory().createGlobalSettings();
        return settings;
    }

    /**
     * Get the settings key for a provided space
     * @param key       The space key
     * @param configKey The configuration key
     * @return The compiled settings key
     */
    private static String getSettingsKey(String key, ConfigKey configKey) {
        return String.format("%s.%s.%s", QRLinkComponent.getInstance().getName(), key, configKey.name());
    }

    /**
     * Set the URL shortener service for the provided space to use
     * @param key    The space key
     * @param domain The URL shortener domain
     */
    public static void setShortener(String key, String domain) {
        getSettings().put(getSettingsKey(key, ConfigKey.URLSHORTENER), domain);
    }

    /**
     * Get the URL shortener service for the provided space
     * @param key The space key
     * @return A URL shortener service
     */
    public static Shortener getShortener(String key) {
        String shortener = (String) getSettings().get(getSettingsKey(key, ConfigKey.URLSHORTENER));
        if (shortener == null || shortener.isEmpty()) {
            setShortener(key, Shortener.RL24.getValue());
            return Shortener.RL24;
        }
        return Shortener.getShortener(shortener);
    }

}
