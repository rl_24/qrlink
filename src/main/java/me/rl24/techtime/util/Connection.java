package me.rl24.techtime.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Request connection class
 * Handles storing information for constructing an HTTP request
 */
public class Connection {

    private String url, json;
    private Map<String, String> params = new HashMap<>(), headers = new HashMap<>();

    /**
     * Construct a new Connection object
     * @param url The URL of the connection to be constructed
     */
    public Connection(String url) {
        setUrl(url);
    }

    /**
     * Get the URL to make a request to
     * @return The URL to make a request to
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the URL to make a request to
     * @param url The URL to make a request to
     * @return The connection object, when used as a builder method
     */
    public Connection setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get the JSON payload for the connection
     * @return The JSON payload for the connection
     */
    public String getJson() {
        return json;
    }

    /**
     * Set the JSON payload for the connection
     * @param json The JSON payload for the connection
     * @return The connection object, when used as a builder method
     */
    public Connection setJson(String json) {
        this.json = json;
        return this;
    }

    /**
     * Get the parameters of the connection
     * @return The parameters of the connection
     */
    public Map<String, String> getParams() {
        return params;
    }

    /**
     * Set the parameters of the connection
     * @param params The parameters of the connection
     */
    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    /**
     * Get the headers of the connection
     * @return The headers of the connection
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * Set the headers of the connection
     * @param headers The headers of the connection
     */
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    /**
     * Set the User-Agent connection header
     * @param userAgent The user agent to use
     * @return The connection object, when used as a builder method
     */
    public Connection setUserAgent(String userAgent) {
        params.put("User-Agent", userAgent);
        return this;
    }

    /**
     * Set the Accept connection header
     * @param accept The accept type to use
     * @return The connection object, when used as a builder method
     */
    public Connection setAccept(String accept) {
        params.put("Accept", accept);
        return this;
    }

    /**
     * Set the Content-Type connection header
     * @param contentType The content type to use
     * @return The connection object, when used as a builder method
     */
    public Connection setContentType(String contentType) {
        params.put("Content-Type", contentType);
        return this;
    }

    /**
     * Add parameters to the connection parameters
     * @param params The parameters to add, e.g. ["email", "admin@domain.tld", "password", "myawesomepassword101"]
     * @return The connection object, when used as a builder method
     */
    public Connection setParams(String... params) {
        return fillMap(this.params, params);
    }

    /**
     * Add headers to the connection headers
     * @param headers The headers to add, e.g. ["Content-Type", "text/html", "Accept", "application/json"]
     * @return The connection object, when used as a builder method
     */
    public Connection setHeaders(String... headers) {
        return fillMap(this.headers, headers);
    }

    /**
     * Add to a map with the given arguments
     * @param map  The map to append
     * @param args The arguments to append, e.g. ["key1", "value1", "key2", "value2"]
     * @return The connection object, when called from a builder method
     */
    private Connection fillMap(Map<String, String> map, String... args) {
        for (int i = 0; i < args.length; i++)
            map.put(args[i], args[i++]);
        return this;
    }

    /**
     * Get the generated payload of the connection, constructed by either the provided JSON body, or the parameters applied
     * @return The generated payload of the connection
     */
    public String getPayload() {
        if (getJson() != null)
            return json;
        StringBuilder payload = new StringBuilder();
        getParams().forEach((k, v) -> payload.append(String.format("%s=%s", k, v)));
        return payload.toString();
    }

}
