package me.rl24.techtime.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Request helper class
 * Handle creating HTTP requests
 */
public class Connector {

    /**
     * The method of the HTTP request
     */
    public enum Method {
        GET,
        POST
    }

    /**
     * Read the content from a provided HTTP connection
     * @param conn     The connection to create an input reader from
     * @param response The appended response string, split by new lines
     */
    private static void readStream(HttpURLConnection conn, StringBuilder response) {
        try {
            InputStream stream = conn.getResponseCode() == 200 ? conn.getInputStream() : conn.getErrorStream();
            if (stream == null)
                throw new IOException();
            BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = buffer.readLine()) != null)
                response.append(line).append("\n");
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handle getting the response from a provided request connection and method
     * @param conn   The connection to get a response from
     * @param method The method of connection
     * @return The HTTP connection response
     */
    private static String request(Connection conn, Method method) {
        try {
            String payload = conn.getPayload();
            URL url = new URL(conn.getUrl() + (method == Method.GET ? (payload.isEmpty() ? "" : String.format("?%s", payload)) : ""));
            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod(method.name());
            conn.getHeaders().forEach(urlConn::setRequestProperty);
            if (method == Method.POST) {
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);

                DataOutputStream dos = new DataOutputStream(urlConn.getOutputStream());
                dos.writeBytes(payload);
                dos.flush();
                dos.close();
            }

            StringBuilder response = new StringBuilder();
            readStream(urlConn, response);

            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Make a GET request
     * @param conn The connection data
     * @return The response from the request
     */
    public static String get(Connection conn) {
        return request(conn, Method.GET);
    }

    /**
     * Make a POST request
     * @param conn The connection data
     * @return The response from the request
     */
    public static String post(Connection conn) {
        return request(conn, Method.POST);
    }

}
