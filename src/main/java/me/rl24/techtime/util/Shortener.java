package me.rl24.techtime.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

/**
 * URL shortener
 * Handles the requests for shortening URLs
 */
public enum Shortener {
    RL24("rl24.me", (url) -> {
        try {
            url = String.format("https://rl24.me/api/shorten/?url=%s", URLEncoder.encode(url, StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return url;
        }
        return Connector.get(new Connection(url));
    }, (res) -> {
        JsonParser parser = new JsonParser();
        JsonElement jel = parser.parse(res);
        JsonObject job = jel.getAsJsonObject();

        if (job.has("url"))
            return job.get("url").getAsString();

        return job.get("error").getAsJsonObject().get("reason").getAsString();
    }),
    TNY_IM("tny.im", (url) -> {
        url = String.format("https://tny.im/yourls-api.php?action=shorturl&format=simple&url=%s", url);
        return Connector.get(new Connection(url));
    }, (res) -> res);

    /**
     * Returns the selected URL shortener.
     * Defaults to rl24.me due to issues with is.gd/v.gd and cloudflare, and goo.gl being shut down
     * @param value Shortener domain
     * @return URL shortener
     */
    public static Shortener getShortener(String value) {
        for (Shortener shortener : Shortener.values())
            if (shortener.getValue().equalsIgnoreCase(value))
                return shortener;
        return RL24;
    }

    String value;
    Function<String, String> requestHandler, responseHandler;

    /**
     * Construct a new shortener
     * @param value           The value used in the Atlassian plugin macro parameter
     * @param requestHandler  The request handler
     * @param responseHandler The response handler
     */
    Shortener(String value, Function<String, String> requestHandler, Function<String, String> responseHandler) {
        this.value = value;
        this.requestHandler = requestHandler;
        this.responseHandler = responseHandler;
    }

    /**
     * Shorten a given URL by the provided URL shortener
     * @param url The URL to be shortened
     * @return The shortened URL
     */
    public String shortenUrl(String url) {
        return responseHandler.apply(requestHandler.apply(url));
    }

    /**
     * Get the value of the shortener service
     * @return The value of the shortener service
     */
    public String getValue() {
        return value;
    }
}
