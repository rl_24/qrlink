package me.rl24.techtime.macro;

import com.atlassian.confluence.content.render.image.ImageDimensions;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.*;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.Map;

/**
 * Confluence QRLink Macro class
 */
public class QRLinkMacro implements Macro, EditorImagePlaceholder, ResourceAware {

    /**
     * Execute the creation of the Macro
     * @param map               The Macro parameter map
     * @param s                 The body of the Macro
     * @param conversionContext The conversion context of the Macro
     * @return The renderable content for the Macro
     * @throws MacroExecutionException
     */
    @Override
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
        QRLink qr = new QRLink(conversionContext, map.get("URL"), map.get("Width"), map.get("Height"), map.get("Alignment"));

        Map context = MacroUtils.defaultVelocityContext();
        context.put("qr", qr);

        return VelocityUtils.getRenderedTemplate("templates/qrlink.vm", context);
    }

    /**
     * Get the body type of the Macro
     * @return The body type of the Macro
     */
    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    /**
     * Get the output type of the Macro
     * @return The output type of the Macro
     */
    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    /**
     * Generate a default QR code for the Macro placeholder when editing a page
     * @param map               The Macro parameter map
     * @param conversionContext The conversion context of the Macro
     * @return The image placeholder for the Macro
     */
    @Override
    public ImagePlaceholder getImagePlaceholder(Map<String, String> map, ConversionContext conversionContext) {
        String width = map.get("Width");
        String height = map.get("Height");
        int w = 150;
        int h = 150;
        try {
            if (width != null)
                w = Integer.parseInt(width);
            if (height != null)
                h = Integer.parseInt(height);
        } catch (Exception e) {
        }
        return new DefaultImagePlaceholder(String.format("https://api.qrserver.com/v1/create-qr-code/?size=%sx%s&data=Default", w, h), false, new ImageDimensions(w, h));
    }

    /**
     * Get the resource path of the default image placeholder
     * @return The resource path of the default image placeholder
     */
    @Override
    public String getResourcePath() {
        return null;
    }

    /**
     * Set the resource path of the default image placeholder
     * @param s The new resource path of the default image placeholder
     */
    @Override
    public void setResourcePath(String s) {
    }
}
