package me.rl24.techtime.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.spring.container.ContainerManager;
import me.rl24.techtime.util.Settings;
import me.rl24.techtime.util.Shortener;

import java.util.UUID;

/**
 * QRLink information class
 * Contains information about a generated QR link
 */
public class QRLink {

    private String id, url, align, shortenedUrl;
    private int width = 150;
    private int height = 150;

    /**
     * Construct a new QRLink object
     * @param conversionContext The Macro's conversion context
     * @param url               The URL to create a QR code for
     * @param width             The width of the QR code
     * @param height            The height of the QR code
     * @param align             The QR code's rendered alignment on the page
     */
    public QRLink(ConversionContext conversionContext, String url, String width, String height, String align) {
        this.id = UUID.randomUUID().toString();
        this.url = url;

        // Capture the current page and use that as the QR generation URL, and append the QR codes ID anchor
        if (this.url == null || this.url.isEmpty()) {
            SettingsManager sm = (SettingsManager) ContainerManager.getComponent("settingsManager");
            ContentEntityObject ceo = conversionContext.getEntity();
            if (ceo != null)
                this.url = sm.getGlobalSettings().getBaseUrl() + ceo.getUrlPath();
            this.url+= "#" + this.id;
        }

        this.align = align;
        this.shortenedUrl = Settings.getShortener(conversionContext.getSpaceKey()).shortenUrl(this.url);

        if (this.align == null)
            this.align = "Left";

        try {
            if (width != null)
                this.width = Integer.parseInt(width);
            if (height != null)
                this.height = Integer.parseInt(height);
        } catch (Exception e) {}
    }

    /**
     * Get the ID of the QR link
     * @return The ID of the QR link
     */
    public String getId() {
        return id;
    }

    /**
     * Get the full URL of the QR link
     * @return The full URL of the QR link
     */
    public String getUrl() {
        return url;
    }

    /**
     * Get the alignment of the QR link in the page block
     * @return The block alignment of the QR link
     */
    public String getAlign() {
        return align.toLowerCase();
    }

    /**
     * Get the shortened version of the full URL
     * @return The shortened URL
     */
    public String getShortenedUrl() {
        return shortenedUrl;
    }

    /**
     * Get the width of the QR code
     * @return The width of the QR code
     */
    public int getWidth() {
        return width;
    }

    /**
     * Get the height of the QR code
     * @return The height of the QR code
     */
    public int getHeight() {
        return height;
    }

}
