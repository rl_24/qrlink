package me.rl24.techtime.action;

import com.atlassian.confluence.spaces.actions.SpaceAdminAction;
import me.rl24.techtime.util.Settings;

/**
 * Space tools configuration action
 */
public class ConfigAction extends SpaceAdminAction {

    public String shortener;

    /**
     * Config page load method
     * @return The page load type
     */
    public String doCreate() {
        if (shortener != null && !shortener.isEmpty())
            Settings.setShortener(getSpaceKey(), shortener);
        else
            shortener = Settings.getShortener(getSpaceKey()).getValue();
        return SUCCESS;
    }

}
