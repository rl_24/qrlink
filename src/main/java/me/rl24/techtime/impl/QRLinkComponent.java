package me.rl24.techtime.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import me.rl24.techtime.api.IQRLinkComponent;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Generated classes from 'atlas-create-confluence-plugin'
 */
@ExportAsService({IQRLinkComponent.class})
@Named("qrLinkComponent")
public class QRLinkComponent implements IQRLinkComponent {

    /**
     * Create a singleton from the component for use in other classes
     */
    private static QRLinkComponent instance;
    public static QRLinkComponent getInstance() {
        return instance;
    }

    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public QRLinkComponent(final ApplicationProperties applicationProperties, final PluginSettingsFactory pluginSettingsFactory) {
        instance = this;
        this.applicationProperties = applicationProperties;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getName() {
        if (applicationProperties != null)
            return "qrLinkComponent:" + applicationProperties.getDisplayName();

        return "qrLinkComponent";
    }

    public PluginSettingsFactory getPluginSettingsFactory() {
        return pluginSettingsFactory;
    }
}