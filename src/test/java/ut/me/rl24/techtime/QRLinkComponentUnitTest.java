package ut.me.rl24.techtime;

import org.junit.Test;
import me.rl24.techtime.api.IQRLinkComponent;
import me.rl24.techtime.impl.QRLinkComponent;

import static org.junit.Assert.assertEquals;

public class QRLinkComponentUnitTest {
    @Test
    public void testMyName() {
        IQRLinkComponent component = new QRLinkComponent(null, null);
        assertEquals("names do not match!", "qrLinkComponent", component.getName());
    }
}