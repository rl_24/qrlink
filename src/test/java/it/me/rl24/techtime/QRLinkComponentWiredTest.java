package it.me.rl24.techtime;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import me.rl24.techtime.api.IQRLinkComponent;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class QRLinkComponentWiredTest {
    private final ApplicationProperties applicationProperties;
    private final IQRLinkComponent qrLinkComponent;

    public QRLinkComponentWiredTest(ApplicationProperties applicationProperties, IQRLinkComponent qrLinkComponent) {
        this.applicationProperties = applicationProperties;
        this.qrLinkComponent = qrLinkComponent;
    }

    @Test
    public void testMyName() {
        assertEquals("names do not match!", "qrLinkComponent:" + applicationProperties.getDisplayName(), qrLinkComponent.getName());
    }
}